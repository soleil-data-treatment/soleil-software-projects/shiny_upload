# shiny-upload

<br>
<img src="src/www/logo_soleil.png" height=200>
<img src="src/www/logo-garlic.jpg" height=200>
<br>

A web service to upload A.I. annotated data sets for e.g. training, built with R-Shiny

:warning: This project is OBSOLETE. Please use to : https://gitlab.com/soleil-data-treatment/soleil-software-projects/data-upload-service

## Description

This is a web service that allows to upload a series of data sets, with 
some metadata input from the web form.

Data sets are then retrieved, and stored locally together with a description file holding some YAML format.

## Installation

On a Debian/Ubuntu system, just type e.g.

```
sudo apt install r-cran-shiny r-cran-stringr
```

get the source code:
```
git clone https://gitlab.com/soleil-data-treatment/soleil-software-projects/shiny_upload
cd shiny_upload
```

You may tune the `src/shiny_upload.R` main file with the storage location, and the maximum file size:
```R
target_srv <- "/tmp"    # e.g. /tmp or /var/www/html
target     <- "data/ai" # will be created if does not exist yet
options(shiny.maxRequestSize=10000*1024^2) # 10 GB
```

Execute the script `run.sh` and connect to http://localhost:3838
```
cd src
./run.sh
```

## Installation on a server

In pratice, to put this service in production, you should specify on an Apache server:
```R
target_srv <- "/var/www/html"
```

Make sure the `data` area is writable for the user running the service, and copy the shiny repo to e.g. `/usr/share/`.
```
sudo mkdir /var/www/html/data
sudo chown www-data /var/www/html/data
sudo cp -r shiny_upload /usr/share/
```

The write the `/etc/systemd/system/shiny_upload.service` start-script:
```
# /etc/systemd/system/shiny_upload.service
[Unit]
Description=File Upload Shiny App
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=www-data
ExecStart=nohup R -e "shiny::runApp('/usr/share/shiny_upload/src/shiny_upload.R', port = 3838, host = '0.0.0.0')" > /var/log/shiny_upload.log 2>&1 &

[Install]
WantedBy=multi-user.target
```

and activate it:
```
sudo systemctl enable shiny_upload
sudo systemctl start  shiny_upload
```

You may optionally configure apache to redirect the port or a virtual folder to the shiny app:
- https://stackoverflow.com/questions/43527041/run-r-shiny-app-on-apache-server

Write the apache configuration file `/etc/apache2/conf-available/hide-shiny-upload-port.conf` to link the virtual folder `/upload` to port :3838
```
# /etc/apache2/conf-available/hide-shiny-upload-port.conf

# make sure the port (here 3838) corresponds with that defined in 
# /etc/systemd/system/shiny_upload.service (ExecStart line)
<Location /upload>
    ProxyPass http://localhost:3838
    ProxyPassReverse http://localhost:3838
</Location>
```

and activate it:
```
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_http2
sudo a2enconf hide-shiny-upload-port.conf
sudo systemctl restart apache2
```

Then the service is available at:
- http://localhost/upload

## Usage

Open e.g.
- http://localhost:3838

Of course, it may also be accessed distantly.

<img src="images/upload-service_form.png" height=500>

Enter the required information, and choose files to upload.

---
(c) 2023 E. Farhi - Synchrotron SOLEIL/GRADES - Powered by R Shiny <img src="images/Shiny-logo.png" height=100>

